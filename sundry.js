// ==UserScript==
// @name sundry
// @namespace my
// @description sundry
// @version 151
// @match *://*/*
// @grant GM_addStyle
// @grant GM_getValue
// @grant GM_setValue
// @grant GM_deleteValue
// @grant GM_listValues
// @grant GM_addValueChangeListener
// @grant GM_openInTab
// @grant GM_download
// @grant GM_notification
// @grant GM_setClipboard
// @grant window.close
// @run-at document-idle
// ==/UserScript==
// xxxxxxxx noframes
"use strict";

var isTouchScreen=('ontouchstart' in window)||(navigator.maxTouchPoints>0)||(navigator.msMaxTouchPoints>0);
var isMobileUA=window.navigator.userAgent.includes('Android');
var leftMouseDownE;
var rightMouseDownE;
var rightMouseDownLink;
var rightMouseDownSelected;
var touchStartE;
var touchStartLink;
var touchStartTime;
addCloseButton();
addgestures();
baiduAD();
watchToDoTesseract();

GM_addStyle('*{user-select:auto !important;}');

NodeList.prototype.remove=function()
{
	for(let x of this){x.remove()}
}

function addCloseButton()
{
	if(isTouchScreen&&!document.querySelector('#xclosebuttonx')) //&&navigator.userAgent.includes('Firefox')
	{
		let x=document.createElement('div');
		x.id='xclosebuttonx';
		x.style=`position:fixed;z-index:9999999999999;top:60vh;left:0px;width:12vw;height:6vh;border-style:solid;border-width:1px;border-color:hotpink`;
		x.onclick=function(){window.close()};
		x.addEventListener('contextmenu',function(e){e.preventDefault();e.stopImmediatePropagation();e.stopPropagation();x.remove()});
		document.body.append(x);
	}
}

function addgestures()
{
	document.addEventListener('contextmenu',function(e)
	{
		if(!e.ctrlKey&&!isTouchScreen)
		{
			e.preventDefault();
			if(e.clientX<3)
			{
				if(e.clientY<(window.innerHeight * 0.5))
				{
					window.scrollBy(0, - window.innerHeight * 0.8);
				}
				else if(e.clientY>(window.innerHeight * 0.5))
				{
					window.scrollBy(0, window.innerHeight * 0.8);
				}
			}
		}
	});
	//right mouse and touch gestures
	let lastClientX;
	let lastClientY;
	let movementXs=[];
	let movementYs=[];
	let canvas;
	let ctx;
	document.addEventListener('mousedown',function(e){
		if(e.which==1){leftMouseDownE=e}
		else if(e.which==3&&!e.ctrlKey)
		{
			rightMouseDownE=e;
			rightMouseDownLink=e.target.closest('a')==null?null:e.target.closest('a');
			rightMouseDownSelected=window.getSelection().toString()==''?null:window.getSelection().toString();
			document.removeEventListener('mousemove',logMouseMove);
			document.addEventListener('mousemove',logMouseMove);
		}
	});
	function logMouseMove(e){
		let dot=document.createElement('div');
		dot.style=`position:fixed;z-index:999999999999999999999999;top:${e.clientY}px;left:${e.clientX}px;width:6px;height:6px;background:deeppink`;
		dot.className='mygesturetrace';
		document.body.append(dot);
		if(lastClientX!=null)
		{
			movementXs.push(e.clientX-lastClientX);
			movementYs.push(e.clientY-lastClientY);
		}
		else{document.querySelectorAll('#xmycontextmenux,#xpastecopiedtextx').remove();}
		/*if(lastClientX==null)
		{
			canvas=document.createElement('canvas');
			canvas.id='mygesturecanvas';
			canvas.width=document.body.clientWidth;
			canvas.height=document.body.clientHeight;
			canvas.style=`position:fixed;left:0px;top:0px;z-index:999999999999999999999999`;
			document.body.append(canvas);
			ctx=canvas.getContext('2d');
			ctx.strokeStyle='deeppink';
			ctx.lineWidth=10;
		}
		else
		{
			document.querySelectorAll('#xmycontextmenux,#xpastecopiedtextx').remove();
			movementXs.push(e.clientX-lastClientX);
			movementYs.push(e.clientY-lastClientY);
			document.querySelector('#xmycontextmenux')?.remove();
			ctx.beginPath();
			ctx.moveTo(lastClientX,lastClientY);
			ctx.lineTo(e.clientX,e.clientY);
			ctx.stroke();
		}*/
		lastClientX=e.clientX;
		lastClientY=e.clientY;
	}
	document.addEventListener('mouseup',function(e){
		if(e.which==1){leftMouseDownE=null;}
		if(e.which==3)
		{
			if(leftMouseDownE)
			{
				if(leftMouseDownE.clientY!=e.clientY){leftMouseDownE=null}
				else
				{
					if(location.host=='fanyi.baidu.com'){GM_setClipboard(document.querySelector('#trans-selection')?.innerText,'text')}
					if(location.href.startsWith('https://www.iciba.com/translate')){GM_setClipboard(document.querySelectorAll('.Text_result__2GDnY')[1].innerText,'text')}
					window.close();
				}
			}
			document.removeEventListener('mousemove',logMouseMove);
			document.querySelectorAll('#mygesturecanvas,.mygesturetrace').remove();
			let path='';
			for(let i=1;i<movementXs.length;i++)
			{
				if(movementXs[i]>0&&movementXs[i]>Math.abs(movementYs[i]))
				{
					path+='r'.repeat(Math.abs(movementXs[i]));
				}
				else if(movementXs[i]<0&&-movementXs[i]>Math.abs(movementYs[i]))
				{
					path+='l'.repeat(Math.abs(movementXs[i]));
				}
				else if(movementYs[i]>0&&movementYs[i]>Math.abs(movementXs[i]))
				{
					path+='d'.repeat(Math.abs(movementYs[i]));
				}
				else if(movementYs[i]<0&&-movementYs[i]>Math.abs(movementXs[i]))
				{
					path+='u'.repeat(Math.abs(movementYs[i]));
				}
				else{}
			}
			movementXs=[];
			movementYs=[];
			lastClientX=null;
			lastClientY=null;
			path=path.replace(/(([a-z])\2{2,})|[a-z]/g,"$1");
			path=path.replace(/([a-z])\1{2,}/g,"$1");
			if(path=='')
			{
				setTimeout(function(){
					if(window.getSelection().toString()!='')(GM_setClipboard(window.getSelection().toString(),'text'))
				},300);
			}
			else{console.log(path)}
			if(rightMouseDownE.target.tagName.toLowerCase()=='textarea'||rightMouseDownE.target.tagName.toLowerCase()=='input'||rightMouseDownE.target.tagName.toLowerCase()=='email'||rightMouseDownE.target.isContentEditable)
			{
				if(path=='d'){GM_setClipboard('usethiskeywordtofakepaste','text')}
			}
			else if(path=='lr')
			{
				ytNotInterested(rightMouseDownE.target);
			}
			else if(path=='rl')
			{
				document.querySelector('#xclickthistoreopenclosedtabx')?.click()
			}
			else if((e.clientX-rightMouseDownE.clientX)>60&&(path=='r'||(path==''&&(e.clientX-rightMouseDownE.clientX)>Math.abs(e.clientY-rightMouseDownE.clientY))))
			{document.querySelector('#xclickthistogotonexttabx')?.click()}
			else if((rightMouseDownE.clientX-e.clientX)>60&&(path=='l'||(path==''&&(rightMouseDownE.clientX-e.clientX)>Math.abs(e.clientY-rightMouseDownE.clientY))))
			{document.querySelector('#xclickthistogotoprevioustabx')?.click()}
			else if((rightMouseDownE.clientY-e.clientY)>60&&(path=='u'||(path==''&&(rightMouseDownE.clientY-e.clientY)>Math.abs(e.clientX-rightMouseDownE.clientX))))
			{
				window.scrollTo(0, 0)
			}
			else if((e.clientY-rightMouseDownE.clientY)>60&&(path=='d'||(path==''&&(e.clientY-rightMouseDownE.clientY)>Math.abs(e.clientX-rightMouseDownE.clientX))))
			{
				window.scrollTo(0,document.body.scrollHeight)
			}
			else if(path=='ud'){location.reload()}
			else if(path=='du'){showDomains()}
			else if(path=='lu')
			{
				if(rightMouseDownE!=null&&rightMouseDownE.target!=null&&rightMouseDownE.target.innerText!=null){GM_setClipboard(rightMouseDownE.target.innerText,'text')}
			}
			else if(path=='lurd'){GM_setClipboard(document.body.innerText,'text')}
			else if(path=='ld'&&rightMouseDownLink!=null&&rightMouseDownLink.href!=null){GM_setClipboard(rightMouseDownLink.href,'text')}
			else if(path=='ru')
			{
				if(rightMouseDownSelected!=null)
				{
					GM_openInTab(`https://www.baidu.com/s?ie=UTF-8&wd=`+encodeURIComponent(rightMouseDownSelected),false);
				}
				else if(rightMouseDownE!=null&&rightMouseDownE.target!=null&&rightMouseDownE.target.innerText!=null)
				{
					GM_openInTab(`https://www.baidu.com/s?ie=UTF-8&wd=`+encodeURIComponent(rightMouseDownE.target.innerText),false);
				}
			}
			else if(path=='rd')
			{
				if(rightMouseDownSelected)
				{
					GM_openInTab('https://fanyi.baidu.com/translate?query='+encodeURIComponent(rightMouseDownSelected),false);
				}
				else if(rightMouseDownE?.target?.closest('a')?.href?.startsWith('https://m.youtube.com'))
				{
					GM_openInTab(rightMouseDownE.target.closest('a').href.replace('m.youtube.com','www.youtube.com'),false);
				}
				else if(rightMouseDownE?.target?.innerText)
				{
					GM_openInTab('https://fanyi.baidu.com/translate?query='+encodeURIComponent(rightMouseDownE.target.innerText),false);
				}
			}
			else if(path=='dudu')
			{
				if(rightMouseDownE.target.closest('img'))
				{
					GM_openInTab('file:///home/spot/extensions/0us/blank_tesseract.html?tesseractimg1src='+encodeURIComponent(getJpegDataUrl(rightMouseDownE.target.closest('img')))+'&language=eng',{active:true,setParent:true})
				}
			}
			else if(path=='dududu')
			{
				if(rightMouseDownE.target.closest('img'))
				{
					GM_openInTab('file:///home/spot/extensions/0us/blank_tesseract.html?tesseractimg1src='+encodeURIComponent(getJpegDataUrl(rightMouseDownE.target.closest('img')))+'&language=chi_sim',{active:true,setParent:true})
				}
			}
			else if(path=='rdlu')
			{
				getClash();
			}
			else if(path=='rlrl'){showSelector(rightMouseDownE.target)}
			else if(path=='dud'){rightMouseDownE.target.remove()}
			rightMouseDownE=null;
			rightMouseDownLink=null;
			rightMouseDownSelected=null;
		}
	});
	//touch
	if(isTouchScreen)
	{
		document.addEventListener('touchstart',function(e){
			touchStartTime=Date.now();
			touchStartE=e;
			if(e.target.closest('a')!=null){touchStartLink=e.target.closest('a')}
			document.removeEventListener('touchmove',logTouchMove);
			document.addEventListener('touchmove',logTouchMove);
		});
		function logTouchMove(e)
		{
			if(lastClientX!=null&&lastClientY!=null)
			{
				movementXs.push(e.touches[0].clientX-lastClientX);
				movementYs.push(e.touches[0].clientY-lastClientY);
			}
			lastClientX=e.touches[0].clientX;
			lastClientY=e.touches[0].clientY;
		}
		document.addEventListener('touchend',function(e){
			document.removeEventListener('touchmove',logTouchMove);
			let path='';
			for(let i=1;i<movementXs.length;i++)
			{
				if(movementXs[i]>0&&movementXs[i]>Math.abs(movementYs[i]))
				{
					path+='r'.repeat(Math.abs(movementXs[i]));
				}
				else if(movementXs[i]<0&&-movementXs[i]>Math.abs(movementYs[i]))
				{
					path+='l'.repeat(Math.abs(movementXs[i]));
				}
				else if(movementYs[i]>0&&movementYs[i]>Math.abs(movementXs[i]))
				{
					path+='d'.repeat(Math.abs(movementYs[i]));
				}
				else if(movementYs[i]<0&&-movementYs[i]>Math.abs(movementXs[i]))
				{
					path+='u'.repeat(Math.abs(movementYs[i]));
				}
				else{}
			}
			path=path.replace(/(([a-z])\2{3,})|[a-z]/g,"$1");
			path=path.replace(/([a-z])\1{3,}/g,"$1");
			if(path=='rl'&&(Date.now()-touchStartTime)<600){mobileGestureOpen(touchStartE.target)}
			else if(path=='lr'){ytNotInterested(touchStartE.target)}
			else if(path=='rlrl'&&(Date.now()-touchStartTime)<1000){showSelector(touchStartE.target)}
			movementXs=[];
			movementYs=[];
			lastClientX=null;
			lastClientY=null;
			touchStartE=null;
			touchStartTime=null;
			touchStartLink=null;
		});
	}
}
function showSelector(e)
{
	let el=e;
	let str='';
	while(el!=document.body)
	{
		let strplus=el.tagName.toLowerCase();
		if(el.id){strplus+='#'+el.id}
		for(let classi of el.classList){strplus+='.'+classi}
		str=strplus+'\n'+str;
		el=el.parentNode;
	}
	let selectorstr=str.replace(/\n/g,' ').trim().replace(/[^#\.\s]+/g,function(x){return CSS.escape(x)});
	let num=document.querySelectorAll(selectorstr).length;
	str+='\n'+num;
	GM_openInTab('data:text/plain,'+encodeURIComponent(str),false);
}
function baiduAD()
{
	if(location.host.endsWith('.baidu.com'))
	{
		GM_addStyle (`div.ec_ad_results, div.ec_ad_results *, div.EC_result, div.EC_result * {background:silver !important}`);
		GM_addStyle (`div[srcid="sigma_celebrity_rela"], div[srcid="sigma_celebrity_rela"] section {background:silver !important}`);
		GM_addStyle (`div[tpl="recommend_list"], div[tpl="recommend_list"] * {background:silver !important}`);
		GM_addStyle (`div.c-clk-recommend, div.c-clk-recommend * {background:silver !important}`);
		GM_addStyle (`div[m-service="relative"], div[m-service="relative"] * {background:silver !important}`);
		GM_addStyle (`div[data-type="rs"], div[data-type="rs"] * {background:silver !important}`);
		GM_addStyle (`div[data-type="ad"], div[data-type="ad"] * {background:silver !important}`);
	}
}
function mobileGestureOpen(etarget)
{
	let inFG=!window.navigator.userAgent.includes('reverseopeninbackground')?false:true;
	if(etarget.closest('a')){GM_openInTab(etarget.closest('a').href,inFG)}
	else if(location.href.match(/^https\:\/\/(www|m)\.baidu\.com\/([^\/]+\/)*s\?/)&&etarget.closest('div.c-result.result'))
	{
		let _href=etarget.closest('div.c-result.result').querySelector('[rl-link-href^="https"]')?.getAttribute('rl-link-href');
		if(_href){GM_openInTab(_href,inFG)};
	}
}

function showDomains()
{
	var domains=[];
	for(let x of document.querySelectorAll('*'))
	{
		if(x.hasAttribute('src')&&x.src!=null&&x.src.match(/https{0,1}\:\/\/.*?\//)!=null)
		{
			let oneDomain=x.src.match(/https{0,1}\:\/\/(.*?)\//)[1];
			if(!domains.includes(oneDomain))
			{
				domains.push(oneDomain);
			}
		}
		if(x.hasAttribute('href')&&x.href!=null&&x.href.toString().match(/https{0,1}\:\/\/.*?\//)!=null&&x.href.toString().match(/https{0,1}\:\/\/(.*?)\//)[1]!=null)
		{
			let oneDomain=x.href.toString().match(/https{0,1}\:\/\/(.*?)\//)[1];
			if(!domains.includes(oneDomain))
			{
				domains.push(oneDomain);
			}
		}
	}
	domains=domains.join('</div><div>');
	domains='<html><div>'+domains+'</div></html>';
	GM_openInTab('data:text/html;base64,'+btoa(domains),false);
}

function ytNotInterested(target)
{
	if(location.hostname.endsWith('.youtube.com')&&target.closest('ytm-rich-item-renderer')&&target.closest('ytm-rich-item-renderer').querySelector('.media-item-menu button'))
	{
		target.closest('ytm-rich-item-renderer').querySelector('.media-item-menu button').click();
		clickNotInterested();
		function clickNotInterested()
		{
			if(document.querySelector('ytm-menu-service-item-renderer .yt-core-attributed-string'))
			{
				document.querySelector('ytm-menu-service-item-renderer .yt-core-attributed-string').click()
			}
			else
			{
				setTimeout(function(){clickNotInterested()},200)
			}
		}
	}
}

function getJpegDataUrl(img)
{
	let canvas = document.createElement('canvas');
	let ctx = canvas.getContext('2d');
	canvas.width = img.width;
	canvas.height = img.height;
	ctx.drawImage(img, 0, 0);
	return canvas.toDataURL('image/jpeg');
}
function doOcrSpace(el,lan)
{
	if(el.tagName!=null&&el.tagName.toLowerCase()=='img')
	{
		let data = new FormData();
		data.append("base64Image", getJpegDataUrl(el)); console.log('image------'+getJpegDataUrl(el));
		data.append("scale",true);
		data.append("language",lan);
		let xhr = new XMLHttpRequest();
		xhr.addEventListener("readystatechange", function(){
			if(xhr.readyState===4&&xhr.responseText.match(/\"ParsedText\"\:\"[^\"]*/)!=null)
			{
				GM_openInTab('data:text/plain;charset=UTF-8,'+encodeURIComponent(JSON.parse(xhr.responseText).ParsedResults[0].ParsedText),false);
			}
		});
		xhr.open("POST", "https://api.ocr.space/parse/image");
		xhr.setRequestHeader("apikey", "8b185b2d1e88957");
		xhr.send(data);
	}
}
function watchToDoTesseract()
{
	if(location.href.startsWith('file:///home/spot/extensions/0us/blank_tesseract.html'))
	{
		let src1;
		if(location.href.match(/tesseractimg1src=([^&]+)/)!=null){src1=location.href.match(/tesseractimg1src=([^&]+)/)[1];src1=decodeURIComponent(src1);}
		let src2;
		if(location.href.match(/tesseractimg2src=([^&]+)/)!=null){src2=location.href.match(/tesseractimg2src=([^&]+)/)[1];src2=decodeURIComponent(src2);}
		let txt;
		if(location.href.match(/attachedtext=([^&]+)/)!=null){txt=location.href.match(/attachedtext=([^&]+)/)[1];txt=decodeURIComponent(txt);}
		let lang;
		if(location.href.match(/language=([^&]+)/)!=null){lang=location.href.match(/language=([^&]+)/)[1]}
		if(src1){document.body.innerHTML+=`<img src="${src1}"></img)`}
		if(src2){document.body.innerHTML+=`<br><img src="${src2}"></img)`}
		let ret1;
		let ret2;
		let scriptnode=document.createElement('script');
		document.body.append(scriptnode);
		scriptnode.onload=function()
		{
			(async ()=>{
				if(src1)
				{
					let worker=await Tesseract.createWorker(lang);
					let ret=await worker.recognize(src1);
					ret1=ret.data.text;
					await worker.terminate();
					if(src2)
					{
						let worker=await Tesseract.createWorker(lang);
						let ret=await worker.recognize(src2);
						ret2=ret.data.text;
						await worker.terminate();
					}
					let str=ret1;
					if(ret2){str=ret2+' '+ret1}
					if(txt){str=txt+' '+str}
					str=str.replace(/\s*$/,'').replace(/\s+/g,' ');
					GM_setClipboard(str,'text');
					document.body.innerHTML+='<br>'+str;
					if(txt){window.close()}//GM_notification({title:'OCR',text:str,timeout:3000,highlight:true})
				}
			})();
		}
		scriptnode.src='https://cdn.jsdelivr.net/npm/tesseract.js@5/dist/tesseract.min.js';
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
function getClash()
{
	document.querySelectorAll('script').remove(); //these scripts may keep refreshing the page content
	if(location.href.startsWith('https://github.com/changfengoss/pub/tree/main/data/'))
	{
		if(document.body.batchprocessed)
		{
			addToStack();
		}
		else
		{
			batchProcessChangfengoss();
			document.body.batchprocessed=true;
		}
	}
	else if(location.href=='https://www.xrayvip.com/free.txt')
	{
		fetch('https://www.xrayvip.com/free.yaml').then(fetchedstuff=>fetchedstuff.text()).then(h=>{
			document.body.innerText=h;
			location.href='https://www.xrayvip.com/free.txt#'
		});
	}
	else if(rightMouseDownLink!=null)
	{
		fetch(rightMouseDownLink.href).then(x=>x.text()).then(y=>{let str=y.replace(/^.*?(\{.*\}).*$/,'$1'); document.body.innerHTML+='<div style="white-space: pre-wrap">'+str+'</div>'});
	}
	else if(document.querySelector('#read-only-cursor-text-area')!=null&&!document.querySelector('#read-only-cursor-text-area').value.includes('server'))
	{
		let x=base64ToClash(document.querySelector('#read-only-cursor-text-area').value);
		document.body.innerHTML='<div style="white-space: pre-wrap">'+x+'</div>';
	}
	else if(document.body.innerText.match(/\{.*server.*\}/)==null)
	{
		let x=base64ToClash(document.body.innerText);
		document.body.innerHTML='<div style="white-space: pre-wrap">'+x+'</div>';
	}
	else
	{
		addToStack()
	}
}
function addToStack()
{
	let txt='';
	if(document.body.innerText.match(/\{.*server.*\}/)!=null)
	{
		txt=document.body.innerText;
	}
	else
	{
		for(let x of document.querySelectorAll('textarea'))
		{
			txt+='\n'+x.value;
		}
	}
	txt=txt.replace(/[\s\S]*?(\n[\s-]*{[^\{\}]*server)/,'$1').replace(/\}[^\{\}]*$/,'');
	document.body.innerHTML='<div style="white-space: pre-wrap">'+txt+'</div>';
	let str='usethiskeywordtostack-'+'\n  '+ document.body.innerText.replace(/\n/g,'  ') + '\n';
	GM_setClipboard(str,'text');
}
function batchProcessChangfengoss()
{
	let itemsSelector='td.react-directory-row-name-cell-large-screen a.Link--primary'; //have to re-query below as links dynamically gone and come back
	for(let i=0; i<document.querySelectorAll(itemsSelector).length;i++)
	{
		fetch(document.querySelectorAll(itemsSelector)[i].href.replace(/.*(\/[^\/]+?\/[^\/]+)/,'https://raw.githubusercontent.com/changfengoss/pub/main/data$1')).then(fetchedstuff=>fetchedstuff.text()).then(h=>{
			let eltext=h;
			if(eltext.includes('server')&&eltext.includes('{')&&eltext.includes('}'))
			{
				eltext=eltext.replace(/^[^\{]*/,'').replace(/[^\}]*$/,'').replace(/\t/g,'  ').replace(/\n/g,'  ');
				document.body.innerHTML+='<div style="white-space: pre-wrap">'+eltext+'</div> <div><br><br><br></div>';
				document.querySelectorAll(itemsSelector)[i].style.background='pink';
			}
			else if(eltext.includes('server'))
			{
				document.querySelectorAll(itemsSelector)[i].style.background='tan';
			}
			else
			{
				try
				{
					let result=base64ToClash(eltext);
					document.body.innerHTML+='<div style="white-space: pre-wrap">'+result+'</div> <div><br><br><br></div>';
					document.querySelectorAll(itemsSelector)[i].style.background='pink';
				}
				catch
				{
					document.querySelectorAll(itemsSelector)[i].style.background='grey';
				}
			}
		});
	}
}
function base64ToClash(inputtext)
{
		let x=''; let result='';
		if(inputtext.match(/^\s*\w{2,6}\:\/\//)!=null)
		{
			x=inputtext
		}
		else
		{
			x=atob(inputtext.replace(/\s/g,''));
		}
		x=x.replace(/[\s]/g,'\n').replace(/\n+/g,'\n')+'\n';
		while(x.match(/\w/)!=null)
		{
			let oneline=x.match(/^(\w{2,6}\:\/\/.*?)\n/);
			if(oneline==null){oneline=''}else{oneline=x.match(/^(\w{2,6}\:\/\/.*?)\n/)[1]}
			if(oneline.startsWith('vmess://'))
			{
				if(oneline.includes('?'))
				{
					let clashob={name:'x',type:'vmess'};
					let cipher_uuid_server_port=atob(oneline.replace('vmess://','').replace(/\?.*/,''));
					if(cipher_uuid_server_port.match(/(.*?)\:(.*?)@(.*?)\:(.*)/)!=null)
					{
						clashob.cipher=cipher_uuid_server_port.match(/(.*?)\:(.*?)@(.*?)\:(.*)/)[1];
						clashob.uuid=cipher_uuid_server_port.match(/(.*?)\:(.*?)@(.*?)\:(.*)/)[2];
						clashob.server=cipher_uuid_server_port.match(/(.*?)\:(.*?)@(.*?)\:(.*)/)[3];
						clashob.port=cipher_uuid_server_port.match(/(.*?)\:(.*?)@(.*?)\:(.*)/)[4];
					}
					oneline=oneline.replace(/.*?\?/,'').replace(/remarks\=[^\&]*/,'').replace(/\?ed\=[^\&]*/,'');
					let obfsparam=oneline.match(/obfs[pP]aram=([^\&]+)/);
					let protoparam=oneline.match(/proto[pP]aram=([^\&]+)/);
					oneline=oneline.replace(/obfs[pP]aram\=[^\&]*/,'').replace(/proto[pP]aram\=[^\&]*/,'');
					let theRest=`{"`+oneline.replace(/^\&+/,'').replace(/\&+$/,'').replace(/\=/g,`":"`).replace(/\&+/g,`","`)+`"}`;
					let theRestOb=JSON.parse(theRest);
					let mergedOb={...clashob,...theRestOb};
					if(obfsparam!=null)
					{
						obfsparam=decodeURIComponent(obfsparam[1]);
						if(obfsparam.startsWith('{'))
						{
							obfsparam=obfsparam.replace(/\"/g,'');
						}
						mergedOb['obfs-param']=obfsparam;
					}
					if(protoparam!=null)
					{
						protoparam=decodeURIComponent(protoparam[1]);
						if(protoparam.startsWith('{'))
						{
							protoparam=protoparam.replace(/\"/g,'');
						}
						mergedOb['proto-param']=protoparam; // or protocol-param?
					}
					result+='\n'+JSON.stringify(mergedOb);
				}
				else
				{
					oneline=atob(oneline.replace('vmess://',''));
					let onelineob=JSON.parse(oneline);
					let clashob={name:'x',type:'vmess',cipher:'auto'};
					if(onelineob.hasOwnProperty('add')){clashob.server=onelineob.add}
					if(onelineob.hasOwnProperty('port')){clashob.port=onelineob.port}
					if(onelineob.hasOwnProperty('id')){clashob.uuid=onelineob.id}
					if(onelineob.hasOwnProperty('aid')){clashob.alterId=onelineob.aid}
					if(onelineob.hasOwnProperty('scy')){clashob.cipher=onelineob.scy}
					if(onelineob.hasOwnProperty('net')){clashob.network=onelineob.net}
					if(onelineob.hasOwnProperty('path')){clashob['ws-opts']={}; clashob['ws-opts'].path=onelineob.path}
					if(onelineob.hasOwnProperty('host')){clashob.headers={}; clashob.headers.host=onelineob.host}
					result+='\n'+JSON.stringify(clashob);
				}
			}
			else if(oneline.startsWith('vless://'))
			{
				oneline=oneline.replace('vless://','');
				let clashob={name:'x',type:'vless',tls:'false',fto:'false',network:'ws'};
				clashob['client-fingerprint']='chrome';
				clashob['skip-cert-verify']=true;
				if(oneline.match(/^[^@]+/)){clashob.uuid=oneline.match(/^[^@]+/)[0]}
				if(oneline.match(/@[^:]+/)){clashob.server=oneline.match(/@([^:]+)/)[1]}
				if(oneline.match(/@[^:]+:\d+/)){clashob.port=oneline.match(/@[^:]+:(\d+)/)[1]}
				if(oneline.match(/host=.*/)){clashob.servername=oneline.match(/host=([^\&]*)/)[1]}
				if(oneline.match(/path=.*/))
				{
					clashob['ws-opts']={path:oneline.match(/path=([^\&]*)/)[1],udp:true};
					if(clashob.servername){clashob['ws-opts']['headers']='Host: '+clashob.servername}
				}
				result+='\n'+JSON.stringify(clashob);
			}
			else if(oneline.startsWith('ssr://'))
			{
				oneline=oneline.replace('ssr://','').replace(/_.*/,'');
				oneline=atob(oneline);
				let server_port_protocol_cipher_obfs_password=oneline.match(/(.*?)\:(.*?)\:(.*?)\:(.*?)\:(.*?)\:(.*?)\//);
				let obfsparam=oneline.match(/obfs[pP]aram=([^\&]+)/);
				let protoparam=oneline.match(/proto[pP]aram=([^\&]+)/);
				let clashob={name:'x',type:'ssr'};
				if(server_port_protocol_cipher_obfs_password!=null)
				{
					clashob.server=server_port_protocol_cipher_obfs_password[1];
					clashob.port=server_port_protocol_cipher_obfs_password[2];
					clashob.protocol=server_port_protocol_cipher_obfs_password[3];
					clashob.cipher=server_port_protocol_cipher_obfs_password[4];
					clashob.obfs=server_port_protocol_cipher_obfs_password[5];
					clashob.password=atob(server_port_protocol_cipher_obfs_password[6]);
				}
				if(obfsparam!=null){clashob['obfs-param']=atob(decodeURIComponent(obfsparam[1].replace(/\$/,'')))}
				if(protoparam!=null){clashob['protocol-param']=atob(decodeURIComponent(protoparam[1].replace(/\$/,'')))}
				result+='\n'+JSON.stringify(clashob);
			}
			else if(oneline.startsWith('ss://'))
			{
				oneline=oneline.replace('ss://','').replace(/#.*/,'').replace(/\?.*/,'');
				let cipher=null;
				let password=null;
				let server=null;
				let port=null;
				let clashob={name:'x',type:'ss'};
				if(oneline.match(/@[-_\w\.]+\:\d+/)==null)
				{
					oneline=atob(oneline);
					cipher=oneline.match(/[^\:]+/);
					password=oneline.match(/.*?\:(.*)@/)[1];
					server=oneline.match(/@(.*?)\:/)[1];
					port=oneline.match(/@.*?\:(.*)/)[1];
				}
				else
				{
					server=oneline.match(/@(.*?)\:(\d+)/)[1];
					port=oneline.match(/@(.*?)\:(\d+)/)[2];
					oneline=oneline.match(/[^@]+/);
					oneline=atob(oneline);
					cipher=oneline.match(/[^\:]+/);
					password=oneline.match(/.*?\:(.*)/)[1];
				}
				if(cipher!=null){clashob.cipher=cipher[0]}
				if(password!=null){clashob.password=password}
				if(server!=null){clashob.server=server}
				if(port!=null){clashob.port=port}
				result+='\n'+JSON.stringify(clashob);
			}
			else if(oneline.startsWith('trojan://'))
			{
				oneline=oneline.replace('trojan://','').replace(/#.*/,'');
				let password=oneline.match(/(.*?)@/);
				let server=oneline.match(/@(.*?)\:/);
				let port=oneline.match(/@.*?\:(\d+)/);
				let sni=oneline.match(/\?sni\=([^#\n]+)/);
				let clashob={name:'x',type:'trojan',udp:true}; clashob['skip-cert-verify']=true;
				if(password!=null){clashob.password=password[1]}
				if(server!=null){clashob.server=server[1]}
				if(port!=null){clashob.port=port[1]}
				if(sni!=null){clashob.sni=sni[1]}
				result+='\n'+JSON.stringify(clashob);
			}
			else{}
			x=x.replace(/.*?\n/,'');
		}
		return result;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////






